import numpy as np
from scipy import special
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from numpy.random import random
import math
import sys


# B_t = sum(i = 0..n) (n choose i) (1-t)^(n-i) * t^i * B(i)
#source : https://pt.wikipedia.org/wiki/Curva_de_B%C3%A9zier

def bezier_curve(P):

    n = len(P) - 1
    i = np.arange(n + 1)
    B = [special.binom(n, k) for k in i]

    for t in np.linspace(0, 1, N_POINTS):
        B_t = np.power(1 - t, n - i) * np.power(t, i) * B
        yield t, B_t @ P

# Recebe uma Line2D line e um click do mouse mouseevent e
# retorna a menor distancia entre as coordenadas do click
# do mouse e um dos pontos que compoe a curva

def nearest_point_picker(line, mouseevent):

        if mouseevent.xdata is None: #out of canvas
            return False, dict()
        d = (line.get_xdata() - mouseevent.xdata)**2 + (line.get_ydata() - mouseevent.ydata)**2
        minimun = np.min(d)
        point = dict(minimun = minimun, line = line)
        return True, point

# Recebe um evento registrado por nearest_point_picker
# e pinta a curva com a menor distancia do ponto clicado

def nearest_line(event):

    event.line.set_c('r')
    global list_full
    list_full.append([event.minimun, event.line])
    if (len(list_full) == N_CURVES):
        fig.canvas.draw()
        a = min(list_full, key = lambda t: t[:][0])
        a[1].set_c('g')
        fig.canvas.draw()
        list_full = []

if (len(sys.argv) != 3): print ("Usage: python3 ep1-8941064 <number of curves> <points per curve> ")

else:

    N_CURVES = int(sys.argv[1])
    N_POINTS = int(sys.argv[2])

    fig, ax = plt.subplots()
    ax.set(xlabel='x(t)', ylabel='y(t)', title = 'Conjunto de curvas')
    ax.grid()

    list_full = []

    for k in range (0, int(N_CURVES / 2)):
        P = np.array([(random(), random()), (random(), random()), (random(), random())])
        curve = np.array([p for _, p in bezier_curve(P)])
        ax.plot(curve[:, 0], curve[:, 1],'r', picker=nearest_point_picker, linewidth=(3.0 / (math.log(N_CURVES) + 0.5)))

    for k in range (int(N_CURVES / 2), N_CURVES):
        P = np.array([(random(), random()), (random(), random()), (random(), random()), (random(), random())])
        curve = np.array([p for _, p in bezier_curve(P)])
        ax.plot(curve[:, 0], curve[:, 1],'r', picker=nearest_point_picker, linewidth=(3.0 / (math.log(N_CURVES) + 0.5)))

    fig.canvas.mpl_connect('pick_event', nearest_line)

    plt.show()